var request = require('request');

module.exports.envia = function (json) {
  return new Promise(function(resolve, reject) {
    var options = {
      url : 'https://app.gabssa.net/sms/carga/enviar-sms',
      method : 'POST',
      json : json,
      rejectUnauthorized : false
    }

    request(options, function(err, res){
      if (err) {
        resolve({err:true, description:err})
      } else {
        resolve(res.body)
      }
    })
  })
}
