conn = null;

sql = require('mssql');

Promise = require('bluebird');

var fs = require('fs');
var sql_cred = JSON.parse(fs.readFileSync(__dirname + '/cred'));

module.exports.start = function(){
	return new Promise(function(resolve, reject){

		var p1 = new Promise(function(resolve, reject){

			conn = new sql.Connection(sql_cred, function (err){
				if(err){
					console.log("Ocurrio un error en el servidor1");
					reject();
				}else{
					console.log("conectado mssql");
					resolve();
				}
			});
		});

		Promise.settle([p1])
		.then(function(results){
			resolve({err: false});
		});

	});
};
