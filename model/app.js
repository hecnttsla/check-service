// var conn = require('../models/main')
var fs = require("fs")
var sql = require("mssql")

module.exports = {
  check: check
}

function check(d) {
  return new Promise(function (resolve, reject){
    var config = JSON.parse(fs.readFileSync(__dirname + '/cred', 'utf8'));
    sql.close();
    sql.connect(config, function (err) {
      if (err) {
        resolve({ err: true, description: err })
      } else {
        var request = new sql.Request();
        var query = `
          INSERT INTO
            gabssa.dbo.serviceVerification
          VALUES
            ('${d.server}', '${d.service}', getDate(), '${d.result}', '${d.result1}')
        `
        request.query(query, function (err, recordset) {
            if (err) {
              resolve({ err: true, description: err })
            } else {
              resolve({ err: false })
            }
          });
      }
    });

  })
}
