var services = require('./services/app')

services.verificarPing()
.then(function(r1){

  services.verificarSms()
  .then(function(r2){

    services.verificarCorreo()
    .then(function(r3){

      services.verificarFtp()
      .then(function(r4){

        services.verificarSql()
        .then(function(r5){

          process.exit()

        })

      })

    })

  })

})
