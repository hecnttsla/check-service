var ping = require('ping');
var model = require('../model/app');
var celular = require('../modules/celular');
var email = require('../modules/email');
var Client = require('ftp');
var JSFtp = require('jsftp');
var fs = require('fs');
var sql = require('mssql');


module.exports = {
  verificarPing: verificarPing,
  verificarSms: verificarSms,
  verificarCorreo: verificarCorreo,
  verificarFtp: verificarFtp,
  verificarSql: verificarSql
}

function verificarPing() {
  return new Promise(function(resolve, reject) {
    var hosts = ['192.168.130.211'];
    hosts.forEach(function(host){
        ping.sys.probe(host, function(isAlive){
          var msg = isAlive ? 'host ' + host + ' is alive' : 'host ' + host + ' is dead';
          var d = {
            result: msg,
            server: hosts[0],
            service: 'PING'
          }
          if (isAlive) {
            d.result1 = 'ACTIVO'
            model.check(d)
            .then(function(res){
              if (res.err) {
                resolve(res)
              } else {
                resolve({err: false, msg: msg})
              }
            })
          } else {
            d.result1 = 'DESACTIVADO'
            correo = 'jess@gabssa.com.mx'
            body = 'Hubo un error al realizar el ping'
            titulo = 'Error servicio'
            email.enviar(correo, body, titulo, []).then(function(){})
            model.check(d)
            .then(function(res){
              if (res.err) {
                resolve(res)
              } else {
                resolve({err: true, msg: msg})
              }
            })
          }
        });
    });
  });
}

function verificarSms() {
  return new Promise(function(resolve, reject) {
    var obj = {
      user : "gabssa_api",
      pass : "4qfRVD",
      telefono : "5535585754",
      mensaje : "Verificar SMS",
      cuenta : "******",
      cliente : "****"
    }
    celular.envia(obj).then(function(res){
      if (res.err) {
        var d = {server: "201.166.145.169", result: "Hubo un error al enviar el sms", service: 'SMS', result1: 'DESACTIVADO'}
        model.check(d)
        .then(function(res){
          correo = 'jess@gabssa.com.mx'
          body = 'Hubo un error al enviar el sms'
          titulo = 'Error servicio'
          email.enviar(correo, body, titulo, []).then(function(){})
          resolve(res)
        })
      } else {
        var d = {server: "201.166.145.169", result: "Canal de SMS activo", service: 'SMS', result1: 'ACTIVO'}
        model.check(d)
        .then(function(res){
          resolve(res)
        })
      }
    })
  });
}

function verificarCorreo() {
  return new Promise(function(resolve, reject) {
    var correo = "johernandez@gabssa.com.mx"
    var body = 'Prueba Correo'
    var titulo = 'Prueba Correo'

    email.enviar(correo, body, titulo, [])
    .then(function(res){
      if (res.err) {
        var d = {server: "200.57.145.142", result: "Hubo un error correo", service: "EMAIL", result1: 'DESACTIVADO'}
        model.check(d)
        .then(function(res){
          correo = 'jess@gabssa.com.mx'
          body = 'Hubo un error correo'
          titulo = 'Error servicio'
          email.enviar(correo, body, titulo, []).then(function(){})
          resolve(res)
        })
      } else {
        var d = {server: "200.57.145.142", result: "Canal Email activo", service: "EMAIL", result1: 'ACTIVO'}
        model.check(d)
        .then(function(res){
          resolve(res)
        })
      }
    })

  });
}

function verificarFtp() {
  return new Promise(function(resolve, reject) {

    var ftp = new JSFtp({
      "host": "200.57.188.1",
      "port": "21",
      "user": "serviceVerification",
      "pass": "SSVer@Ser"
    });

    ftp.on('error', function(err){
      if(err) {
        var d = {server: "200.57.188.1", result: "Canal FTP desactivado", service: "FTP", result1: 'DESACTIVADO'}
        model.check(d)
        .then(function(res){
          correo = 'jess@gabssa.com.mx'
          body = 'Canal FTP desactivado'
          titulo = 'Error servicio'
          email.enviar(correo, body, titulo, []).then(function(){})
          resolve(res)
        })
      }
    })

    ftp.on('data', function(data) {
      var d = {server: "200.57.188.1", result: "Canal FTP activo", service: "FTP", result1: 'ACTIVO'}
      model.check(d)
      .then(function(res){
        resolve(res)
      })
    })

  });
}

function verificarSql() {
  return new Promise(function(resolve, reject) {

    var config = JSON.parse(fs.readFileSync(__dirname + '/cred_1', 'utf8'));

    sql.close();

    sql.connect(config, function (err) {

      if (err) {
        var d = {server: "10.0.0.10", result: "Canal SQL desactivado", service: "SQL", result1: 'DESACTIVADO'}
        model.check(d)
        .then(function(res){
          correo = 'jess@gabssa.com.mx'
          body = 'Canal SQL desactivado'
          titulo = 'Error servicio'
          email.enviar(correo, body, titulo, []).then(function(){})
          resolve(res)
        })
      } else {
        var d = {server: "10.0.0.10", result: "Canal SQL activo", service: "SQL", result1: 'ACTIVO'}
        model.check(d)
        .then(function(res){
          resolve(res)
        })
      }

    });

  });
}
